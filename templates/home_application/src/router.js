import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import Temp from '@/views/temp'

import nav_left_1_1 from '@/views/nav_left_1_1'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   component: Home
    // },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // },
    {
      // path: '/',
      // name: 'Index',
      // component: Index
      path: '/',
      name: 'Temp',
      component: Temp
    },
    {
      path: '/nav_left_1_1',
      // path: '/nav_left_1_1/',
      // path: '/nav_left_1_1/:project',
      // path: '/nav_left_1_1/:project?',//:？容许为空
      name: 'nav_left_1_1',
      component: nav_left_1_1
    },
  ]
})

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import iView from 'iview'
import axios from 'axios'


Vue.prototype.$http = axios
// Vue.prototype.$http.baseURL = "http://192.168.101.109:8001"
// Vue.prototype.$http.defaults.baseURL= 'http://localhost'
// Vue.prototype.$http.defaults.baseURL='http://dev.sgis.com:8000'
// axios.defaults.baseURL = 'api'

axios.interceptors.request.use(config => {
  // console.log('configconfigconfigconfigconfig', config)
  config.headers['X-Requested-With'] = 'XMLHttpRequest';
  let regex = /.*csrftoken=([^;.]*).*$/;
  config.headers['X-CSRFToken'] = document.cookie.match(regex) === null ? null : document.cookie.match(regex)[1];
  // console.log('document.cookie.match(regex)[1]', document.cookie)
  // config.headers.token = document.cookie.match(regex)[1]
  return config
});
axios.interceptors.response.use(response => {
  if (response.status !== 200) {
      return {
          code: response.status,
          message: '请求异常，请刷新重试',
          result: false
      }
  }
  return response.data
}, error => {
  return {
      code: 500,
      message: '未知错误，请刷新重试',
      error: error,
      result: false
  }
});

// Axios.defaults.withCredentials = true
Vue.prototype.$http.defaults.withCredentials = true //携带cookie信息//默认是false


Vue.use(iView)
// Vue.use(vuex)



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

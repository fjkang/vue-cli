# -*- coding: utf-8 -*-

from common.mymako import render_mako_context, render_json
from conf.default import LOGOUT_URL



def home(request):
    """
    首页
    """
    # return render_mako_context(request, '/home_application/home.html')
    # return render_mako_context(request, '/home_application/dist/index.html')
    return render_mako_context(request, '/index.html')


def dev_guide(request):
    """
    开发指引
    """
    return render_mako_context(request, '/home_application/dev_guide.html')


def contactus(request):
    """
    联系我们
    """
    return render_mako_context(request, '/home_application/contact.html')


def test(request):
    return render_json({'result': True, 'data': []})

def login_info(request):
    resp = render_json({
        "result": True,
        "data": {
            "username": request.user.username,
            "logout_url": LOGOUT_URL
        }})
    from django.core.context_processors import csrf
    resp.set_cookie('csrftoken', csrf(request)['csrf_token'])
    return resp